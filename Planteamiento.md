# Cell droid player

El objetivo es crear una aplicación para android que asuma el rol de reproductor de música, que además se puedan organizar todas las carpetas de música en librerias

## Requisitos funcionales de la aplicación

### Entidades

* **Song**: Clase que representa un fichero de audio
* **Library**: Clase que representa una libreria creada por el usuario
* **SongDirectory**: Clase que respresenta un directorio en el sistema de archivos
*  **Player**: Clase estática en la que se estará todo el rato representando la cola de canciones y el archivo actual el cual se esté reproduciendo
* **SongQueue**: Clase que sera encapsulada por player que representara la cola de canciones
* **PlayList**: Clase que representa una lista de reproducción creada por el usuario

### Pantallas

El usuario siempre va a entrar, nada mas abrir la aplicación, a la **pantalla de la última librería** que dejó abierta cuando cerró la aplicación. Tendrá una **barra lateral** y en ella se pondrán todas las librerías que el usuario haya creado. Además, también se mostrarán, para esa librería todas las listas de reproducción que se haya creado para esa librería. La barra lateral, al arrancar la aplicación, aparecerá abierta la primera vez que el usuario arranque la aplicación ya que no habrá ninguna librería creada.

En la **pantalla de vista de ficheros y directorios**, el usuario podrá ir navegando por los ficheros y directorios y reproducir por ejemplo una carpeta, también podrá cambiar los metadatos de las canciones para todo un directorio o una canción, y seleccionar individualmente un archivo para meterlo a una lista de reproducción.

En esa misma pantalla habrá un botón flotante para reproducir todos las canciones que se encuentren presentes en esa carpeta y también se podrá elegir una canción:

* Si se le da al botón flotante se reproducirán todos los archivos de esa carpeta
* Si se le da a una canción en particular, se reproducirán todas las canciones de esa carpeta, empezando por la canción, que el usuario toco

Luego de que el usuario elija que reproducir, llegará a la **pantalla de reproducción**, tendrá todos los botones y todas las funciones que un reproductor común tiene, además de un panel para, en el momento de reproducir la canción, cambiar los metadatos del archivo, y así poder corregir en ese momento los datos de ese archivo si es que el usuario lo necesita.

La pantalla de el reproductor de música da acceso a una pantalla en la que se muestra toda la **cola de canciones** presentes, en ella únicamente se podrá cambiar de orden las canciones.







### Plataformas

El proyecto lo realizaré con las tecnologías .net y presentaré únicamente la versión para android, pero también habrá una versión para escritorio, esto lo haré para ganar tiempo a la hora de probar la aplicación y el código que vaya escribiendo en el ide, de modo que habrá tres proyectos:

* **Proyecto de biblioteca compartidas**: Este será un proyecto en el que se almacenarán todas las apis necesarias para el manejo básico de ficheros que la aplicación necesita manejar.
* **Proyecto de escritorio wpf**: En este proyecto continuamente voy a estar probando el funcionamiento básico de la aplicación, sea por ejemplo cambiar los metadatos de la canción, o reproducir canciones.
* **Proyecto movil**: Este proyecto será el final y el que presente, será el reproductor para android que finalmente, como objetivo quiero hacer.
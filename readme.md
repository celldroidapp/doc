# Documentacion de celldroidapp

Instrucciones para poder visualizar los archivos:

* Archivos **.md**: Son archivos de documentación (como este), que, o bien se pueden ver en git (como este) o bien los puedes abrir o editar con el typora
* Archivos **.xd**: Son archivos para diseñar interfaces, se pueden manejar con el Adobe XD

## Programas útiles

- **Typora**: https://typora.io/
- **Adobe XD**: https://www.adobe.com/es/products/xd.html?sdid=8DN85NTQ&mv=search&s_kwcid=AL!3085!3!315412776687!e!!g!!adobe%20xd&ef_id=W8zS_AAAAJDs7ReW:20181216214221:s